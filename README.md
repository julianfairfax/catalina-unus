[![Please don't upload to GitHub](https://nogithub.codeberg.page/badge.svg)](https://nogithub.codeberg.page)

# Catalina Unus
Catalina Unus is an [open source](#license) command line tool for running macOS Catalina on one HFS or APFS volume. It's integrated into macOS Patcher so you if you have a Mac supported by it, you can create your patched installer using Catalina Unus, and then add macOS Patcher's patches to it by selecting the "Add patches to patched Catalina Unus installer" option.

It's highly recommended to backup your Mac, do a clean install, and migrate the data during setup, cause Catalina Unus has trouble with Catalina's data migration process.

Catalina Unus may not seem to work properly on newer versions of macOS Catalina due to Apple's changes but the patching process should still work. If the installation fails, restart your Mac and follow the patching steps.

## Supported Macs

### iMacs
- iMac13,1
- iMac13,2
- iMac13,3
- iMac14,1
- iMac14,2
- iMac14,3
- iMac14,4
- iMac15,1
- iMac16,1
- iMac16,2
- iMac17,1
- iMac18,1
- iMac18,2
- iMac18,3
- iMac19,1
- iMac19,2

### iMac Pros
- iMacPro1,1

### MacBooks
- MacBook8,1
- MacBook9,1
- MacBook10,1

### MacBook Airs
- MacBookAir5,1
- MacBookAir5,2
- MacBookAir6,1
- MacBookAir6,2
- MacBookAir7,1
- MacBookAir7,2
- MacBookAir8,1
- MacBookAir8,2

### MacBook Pros
- MacBookPro9,1
- MacBookPro9,2
- MacBookPro10,1
- MacBookPro10,2
- MacBookPro11,1
- MacBookPro11,2
- MacBookPro11,3
- MacBookPro11,4
- MacBookPro11,5
- MacBookPro12,1
- MacBookPro13,1
- MacBookPro13,2
- MacBookPro13,3
- MacBookPro14,1
- MacBookPro14,2
- MacBookPro14,3
- MacBookPro15,1
- MacBookPro15,2
- MacBookPro15,3
- MacBookPro15,4

### Mac minis
- Macmini6,1
- Macmini6,2
- Macmini7,1
- Macmini8,1

### Mac Pros
- MacPro6,1
- MacPro7,1

## Supported Versions of macOS
- 10.15 Catalina

## Usage

### Create Installer Drive

#### Step 1

Download the latest version of the patcher from the GitHub releases page.

You can also download it from the Homebrew repository that is now available [here](https://gitlab.com/julianfairfax/package-repo).

#### Step 2

Open Disk Utility and select your installer drive, then click Erase.

#### Step 3

Select Mac OS Extended Journaled and click Erase.

#### Step 4

Unzip the download and open Terminal. Type chmod +x and drag the script file to Terminal, then hit enter. Then type sudo, drag the script file to Terminal, and hit enter.

#### Step 5

Drag your installer app to Terminal. Then select your installer drive from the list and copy and paste the number.

### Erase System Drive (optional)

These Steps are optional. A clean install is recommended but not required. It's recommended to make a Time Machine backup before erasing your system drive.

#### Step 1

Open the Utilities menu and click Disk Utility. Select your system drive and click Erase.

#### Step 2

Select Mac OS Extended Journaled or APFS and click Erase. Don't select APFS if your Mac doesn't support APFS.

### Install the OS

#### Step 1

Boot from your installer drive by holding down the option key when booting and selecting your installer drive from the menu. Then select your language from the list.

#### Step 2

Close Disk Utility if you used it to erase your system drive, then click Continue.

#### Step 3

Select your system drive, the drive to install the OS on, and click Continue.

Wait 35-45 minutes and click Reboot when the installation is complete. Then boot into your installer drive using the previous instructions.

### Patch the OS

This is the important part. This is the part where you install the patcher files onto your system. If you miss this part or forget it then your system won't boot.

#### Step 1

Open the Utilities menu and click Terminal. Type patch and hit enter. Make sure to select the model your drive will be used with. Then select your system drive from the list and copy and paste the number.

Wait 5-10 minutes and reboot when the patch tool has finished patching your system drive. Then boot into your system drive and setup the OS.

### Restore the OS

If you've switched to a new Mac or just want to remove the patcher files from your system, you can run the restore tool from your installer drive.

#### Step 1

Boot from your installer drive by holding down the option key when booting and selecting your installer drive from the menu. Then select your language from the list.

#### Step 2

Open the Utilities menu and click Terminal. Type restore and hit enter. Make sure to select the model you selected when you last patched. Then select your system drive from the list and copy and paste the number.

#### Step 3

You can choose to remove all system patches or the APFS system patch if you have it installed. Don't remove the APFS system patch if your Mac doesn't support APFS.

Wait 5-10 minutes if you selected to remove all system patches, then reinstall the OS. If you selected to remove the APFS system patch, then you don't need to reinstall the OS, and you can boot back into your system drive afterwards

## Docs
If you want to know how Catalina Unus works, you can check [this doc](https://julianfairfax.github.io/docs/patching-macos-to-run-on-unsupported-macs.html) I wrote about patching macOS to run on unsupported Macs and [this one](https://julianfairfax.github.io/docs/patching-macos-to-install-on-unsupported-macs.html) about patching macOS to install on unsupported Macs. You can also review the code yourself and feel free to submit a pull request if you think part of it can be improved.

## License
The following files and folders were created by me and are licensed under the [GNU General Public License v3.0](http://choosealicense.com/licenses/gpl-3.0/):
- Catalina Unus.sh
- resources/InstallerMenuAdditions.plist
- resources/patch-unus.sh

The following files and folders were created by Apple and are licensed under their licenses:
- resources/macOS Installer.app, from 10.12.6 installer
- resources/OSInstaller.framework, from 10.12 beta 1 installer
- resources/Quartz.framework, from 10.12.6 installer
- resources/SystemMigration.framework, from 10.13.6 installer
- resources/SystemMigrationUtils.framework, from 10.13.6 installer
- resources/BaseSystem.002.dmgpart, from 10.14.6 installer
- resources/BaseSystem.003.dmgpart, from 10.14.6 installer
- resources/BaseSystem.004.dmgpart, from 10.14.6 installer
- resources/BaseSystem.005.dmgpart, from 10.14.6 installer
- resources/BaseSystem.dmg, from 10.14.6 installer
- resources/kextcache, from 10.15.7 installer
- resources/X.tiff, from 10.15.7 installer
