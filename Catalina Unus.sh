#!/bin/bash

parameters="${1}${2}${3}${4}${5}${6}${7}${8}${9}"

Escape_Variables()
{
	text_progress="\033[38;5;113m"
	text_success="\033[38;5;113m"
	text_warning="\033[38;5;221m"
	text_error="\033[38;5;203m"
	text_message="\033[38;5;75m"

	text_bold="\033[1m"
	text_faint="\033[2m"
	text_italic="\033[3m"
	text_underline="\033[4m"

	erase_style="\033[0m"
	erase_line="\033[0K"

	move_up="\033[1A"
	move_down="\033[1B"
	move_foward="\033[1C"
	move_backward="\033[1D"
}

Parameter_Variables()
{
	if [[ $parameters == *"-v"* || $parameters == *"-verbose"* ]]; then
		verbose="1"
		set -x
	fi
}

Path_Variables()
{
	script_path="${0}"
	directory_path="${0%/*}"

	resources_path="$directory_path/resources"

	if [[ -d /usr/local/opt/catalin-unus ]]; then
		resources_path="/usr/local/opt/catalina-unus"
	fi
}

Input_Off()
{
	stty -echo
}

Input_On()
{
	stty echo
}

Output_Off()
{
	if [[ $verbose == "1" ]]; then
		"$@"
	else
		"$@" &>/dev/null
	fi
}

Check_Environment()
{
	echo -e $(date "+%b %d %H:%M:%S") ${text_progress}"> Checking system environment."${erase_style}

	if [ -d /Install\ *.app ]; then
		environment="installer"
	fi

	if [ ! -d /Install\ *.app ]; then
		environment="system"
	fi

	echo -e $(date "+%b %d %H:%M:%S") ${move_up}${erase_line}${text_success}"+ Checked system environment."${erase_style}
}

Check_Root()
{
	echo -e $(date "+%b %d %H:%M:%S") ${text_progress}"> Checking for root permissions."${erase_style}

	if [[ $environment == "installer" ]]; then
		root_check="passed"
		echo -e $(date "+%b %d %H:%M:%S") ${move_up}${erase_line}${text_success}"+ Root permissions check passed."${erase_style}
	else

		if [[ $(whoami) == "root" && $environment == "system" ]]; then
			root_check="passed"
			echo -e $(date "+%b %d %H:%M:%S") ${move_up}${erase_line}${text_success}"+ Root permissions check passed."${erase_style}
		fi

		if [[ ! $(whoami) == "root" && $environment == "system" ]]; then
			root_check="failed"
			echo -e $(date "+%b %d %H:%M:%S") ${text_error}"- Root permissions check failed."${erase_style}
			echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/ Run this tool with root permissions."${erase_style}
			Input_On
			exit
		fi

	fi
}

Check_Resources()
{
	echo -e $(date "+%b %d %H:%M:%S") ${text_progress}"> Checking for resources."${erase_style}

	if [[ -d "$resources_path" ]]; then
		resources_check="passed"
		echo -e $(date "+%b %d %H:%M:%S") ${move_up}${erase_line}${text_success}"+ Resources check passed."${erase_style}
	fi

	if [[ ! -d "$resources_path" ]]; then
		resources_check="failed"
		echo -e $(date "+%b %d %H:%M:%S") ${text_error}"- Resources check failed."${erase_style}
		echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/ Run this tool with the required resources."${erase_style}

		Input_On
		exit
	fi
}

Input_Operation()
{
	echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/ What operation would you like to run?"${erase_style}
	echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/ Input an operation number."${erase_style}
	echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/     1 - Patch Catalina Unus installer"${erase_style}
	echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/     2 - Update patches on patched Catalina Unus installer"${erase_style}

	Input_On
	read -e -p "$(date "+%b %d %H:%M:%S") / " operation
	Input_Off

	if [[ $operation == "1" ]]; then
		Input_Installer
		Check_Installer_Stucture
		Check_Installer_Version
		Check_Installer_Support
		Input_Volume
		Create_Installer
		Patch_Installer
	fi

	if [[ $operation == "2" ]]; then
		Input_Volume
		Update_Patched_Installer
	fi

}

Input_Installer()
{
	echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/ What installer would you like to use?"${erase_style}
	echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/ Input an installer path."${erase_style}

	Input_On
	read -e -p "$(date "+%b %d %H:%M:%S") / " installer_application_path
	Input_Off

	installer_application_name="${installer_application_path##*/}"
	installer_application_name_partial="${installer_application_name%.app}"

	installer_sharedsupport_path="$installer_application_path/Contents/SharedSupport"
}

Check_Installer_Stucture()
{
	echo -e $(date "+%b %d %H:%M:%S") ${text_progress}"> Checking installer structure."${erase_style}

		installer_images_path="$installer_sharedsupport_path"

	echo -e $(date "+%b %d %H:%M:%S") ${move_up}${erase_line}${text_success}"+ Checked installer structure."${erase_style}


	echo -e $(date "+%b %d %H:%M:%S") ${text_progress}"> Mounting installer disk images."${erase_style}

		Output_Off hdiutil attach "$installer_sharedsupport_path"/InstallESD.dmg -mountpoint /tmp/InstallESD -nobrowse -noverify
		Output_Off hdiutil attach "$installer_images_path"/BaseSystem.dmg -mountpoint /tmp/Base\ System -nobrowse -noverify

	echo -e $(date "+%b %d %H:%M:%S") ${move_up}${erase_line}${text_success}"+ Mounted installer disk images."${erase_style}
}

Check_Installer_Version()
{
	echo -e $(date "+%b %d %H:%M:%S") ${text_progress}"> Checking installer version."${erase_style}

		installer_version="$(defaults read /tmp/Base\ System/System/Library/CoreServices/SystemVersion.plist ProductVersion)"
		installer_version_short="$(defaults read /tmp/Base\ System/System/Library/CoreServices/SystemVersion.plist ProductVersion | cut -c-5)"

	echo -e $(date "+%b %d %H:%M:%S") ${move_up}${erase_line}${text_success}"+ Checked installer version."${erase_style}	
}

Check_Installer_Support()
{
	echo -e $(date "+%b %d %H:%M:%S") ${text_progress}"> Checking installer support."${erase_style}

	if [[ $installer_version_short == "10.15" ]]; then
		echo -e $(date "+%b %d %H:%M:%S") ${move_up}${erase_line}${text_success}"+ Installer support check passed."${erase_style}
	else
		echo -e $(date "+%b %d %H:%M:%S") ${text_error}"- Installer support check failed."${erase_style}
		echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/ Run this tool with a supported installer."${erase_style}

		Input_On
		exit
	fi
}

Input_Volume()
{
	echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/ What volume would you like to use?"${erase_style}
	echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/ Input a volume number."${erase_style}

	for volume_path in /Volumes/*; do
		volume_name="${volume_path#/Volumes/}"

		if [[ ! "$volume_name" == com.apple* ]]; then
			volume_number=$(($volume_number + 1))
			declare volume_$volume_number="$volume_name"

			echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/     ${volume_number} - ${volume_name}"${erase_style} | sort
		fi

	done

	if [[ "$installer_volume_name" ]]; then
		for volume_path in /Volumes/*; do
			volume_name="${volume_path#/Volumes/}"
	
			if [[ ! "$volume_name" == com.apple* ]]; then
				volumes_number=$(($volumes_number + 1))

				for volume_number in $volumes_number; do
					installer_volume="volume_$volume_number"

					if [[ "${!installer_volume}" == "$installer_volume_name" ]]; then
						installer_volume_number="$volume_number"
					fi

				done
			fi
	
		done

		echo -e $(date "+%b %d %H:%M:%S") "/ $installer_volume_number"${erase_style}
	else
		Input_On
		read -e -p "$(date "+%b %d %H:%M:%S") / " installer_volume_number
		Input_Off

		installer_volume="volume_$installer_volume_number"
		installer_volume_name="${!installer_volume}"
	fi

	installer_volume_path="/Volumes/$installer_volume_name"
	installer_volume_identifier="$(diskutil info "$installer_volume_name"|grep "Device Identifier"|sed 's/.*\ //')"
}

Create_Installer()
{
	echo -e $(date "+%b %d %H:%M:%S") ${text_progress}"> Restoring installer disk image."${erase_style}

		Output_Off asr restore -source "$resources_path"/BaseSystem.dmg -target "$installer_volume_path" -noprompt -noverify -erase

	echo -e $(date "+%b %d %H:%M:%S") ${move_up}${erase_line}${text_success}"+ Restored installer disk image."${erase_style}


	echo -e $(date "+%b %d %H:%M:%S") ${text_progress}"> Renaming installer volume."${erase_style}

		Output_Off diskutil rename /Volumes/macOS\ Base\ System "$installer_volume_name"
		bless --folder "$installer_volume_path"/System/Library/CoreServices --label "$installer_volume_name"

	echo -e $(date "+%b %d %H:%M:%S") ${move_up}${erase_line}${text_success}"+ Renamed installer volume."${erase_style}


	echo -e $(date "+%b %d %H:%M:%S") ${text_progress}"> Copying installer packages."${erase_style}

		rm "$installer_volume_path"/System/Installation/Packages
		cp -R /tmp/InstallESD/Packages "$installer_volume_path"/System/Installation/

	echo -e $(date "+%b %d %H:%M:%S") ${move_up}${erase_line}${text_success}"+ Copied installer packages."${erase_style}


	echo -e $(date "+%b %d %H:%M:%S") ${text_progress}"> Copying installer disk images."${erase_style}

		cp "$installer_images_path"/BaseSystem.dmg "$installer_volume_path"/
		cp "$installer_images_path"/BaseSystem.chunklist "$installer_volume_path"/
	
		cp "$installer_images_path"/AppleDiagnostics.dmg "$installer_volume_path"/
		cp "$installer_images_path"/AppleDiagnostics.chunklist "$installer_volume_path"/

	echo -e $(date "+%b %d %H:%M:%S") ${move_up}${erase_line}${text_success}"+ Copied installer disk images."${erase_style}


	echo -e $(date "+%b %d %H:%M:%S") ${text_progress}"> Unmounting installer disk images."${erase_style}

		Output_Off hdiutil detach /tmp/InstallESD
		Output_Off hdiutil detach /tmp/Base\ System

	echo -e $(date "+%b %d %H:%M:%S") ${move_up}${erase_line}${text_success}"+ Unmounted installer disk images."${erase_style}
}

Patch_Installer()
{
	Patch_Supported
	Patch_Unus
}

Patch_Supported()
{
	echo -e $(date "+%b %d %H:%M:%S") ${text_progress}"> Replacing installer utilities menu."${erase_style}

		cp "$resources_path"/InstallerMenuAdditions.plist "$installer_volume_path"/System/Installation/CDIS/*Installer.app/Contents/Resources

	echo -e $(date "+%b %d %H:%M:%S") ${move_up}${erase_line}${text_success}"+ Replacing installer utilities menu."${erase_style}


	echo -e $(date "+%b %d %H:%M:%S") ${text_progress}"> Patching installer app."${erase_style}

		cp -R "$installer_volume_path"/System/Installation/CDIS/macOS\ Installer.app "$installer_volume_path"/tmp/macOS\ Installer-original.app
		cp -R "$resources_path"/macOS\ Installer.app "$installer_volume_path"/tmp/macOS\ Installer-patched.app
		cp "$installer_volume_path"/tmp/macOS\ Installer-original.app/Contents/Resources/X.tiff "$installer_volume_path"/tmp/macOS\ Installer-patched.app/Contents/Resources/
		cp "$installer_volume_path"/tmp/macOS\ Installer-original.app/Contents/Resources/OSXTheme.car "$installer_volume_path"/tmp/macOS\ Installer-patched.app/Contents/Resources/
		cp "$installer_volume_path"/tmp/macOS\ Installer-original.app/Contents/Resources/ReleaseNameTheme.car "$installer_volume_path"/tmp/macOS\ Installer-patched.app/Contents/Resources/
		cp "$installer_volume_path"/tmp/macOS\ Installer-original.app/Contents/Resources/InstallerMenuAdditions.plist "$installer_volume_path"/tmp/macOS\ Installer-patched.app/Contents/Resources/
		rm -R "$installer_volume_path"/System/Installation/CDIS/macOS\ Installer.app
		cp -R "$installer_volume_path"/tmp/macOS\ Installer-patched.app "$installer_volume_path"/System/Installation/CDIS/macOS\ Installer.app

	echo -e $(date "+%b %d %H:%M:%S") ${move_up}${erase_line}${text_success}"+ Patched installer app."${erase_style}


	echo -e $(date "+%b %d %H:%M:%S") ${text_progress}"> Patching installer framework."${erase_style}

		rm -R "$installer_volume_path"/System/Library/PrivateFrameworks/OSInstaller.framework
		cp -R "$resources_path"/OSInstaller.framework "$installer_volume_path"/System/Library/PrivateFrameworks/

	echo -e $(date "+%b %d %H:%M:%S") ${move_up}${erase_line}${text_success}"+ Patched installer framework."${erase_style}


	if [[ $installer_version_short == "10.1"[4-5] ]]; then
		echo -e $(date "+%b %d %H:%M:%S") ${text_progress}"> Patching system migration frameworks."${erase_style}

			rm -R "$installer_volume_path"/System/Library/PrivateFrameworks/SystemMigration.framework
			rm -R "$installer_volume_path"/System/Library/PrivateFrameworks/SystemMigrationUtils.framework
			cp -R "$resources_path"/SystemMigration.framework "$installer_volume_path"/System/Library/PrivateFrameworks/
			cp -R "$resources_path"/SystemMigrationUtils.framework "$installer_volume_path"/System/Library/PrivateFrameworks/

		echo -e $(date "+%b %d %H:%M:%S") ${move_up}${erase_line}${text_success}"+ Patched system migration frameworks."${erase_style}
	fi


	echo -e $(date "+%b %d %H:%M:%S") ${text_progress}"> Patching graphics driver."${erase_style}

		cp -R "$installer_volume_path"/System/Library/Frameworks/Quartz.framework "$installer_volume_path"/tmp/Quartz-original.framework
		cp -R "$resources_path"/Quartz.framework "$installer_volume_path"/tmp/Quartz-patched.framework
		rm -R "$installer_volume_path"/tmp/Quartz-patched.framework/Versions/A/Frameworks/QuickLookUI.framework
		cp -R "$installer_volume_path"/tmp/Quartz-original.framework/Versions/A/Frameworks/QuickLookUI.framework "$installer_volume_path"/tmp/Quartz-patched.framework/Versions/A/Frameworks/
		rm -R "$installer_volume_path"/System/Library/Frameworks/Quartz.framework
		cp -R "$installer_volume_path"/tmp/Quartz-patched.framework "$installer_volume_path"/System/Library/Frameworks/Quartz.framework

	echo -e $(date "+%b %d %H:%M:%S") ${move_up}${erase_line}${text_success}"+ Patched graphics driver."${erase_style}
}

Patch_Unus()
{
	echo -e $(date "+%b %d %H:%M:%S") ${text_progress}"> Patching installer app."${erase_style}
	
		cp "$resources_path"/10.15/X.tiff "$installer_volume_path"/System/Installation/CDIS/macOS\ Installer.app/Contents/Resources

	echo -e $(date "+%b %d %H:%M:%S") ${move_up}${erase_line}${text_success}"+ Patched installer app."${erase_style}


	echo -e $(date "+%b %d %H:%M:%S") ${text_progress}"> Patching kernel cache tool."${erase_style}

		cp "$resources_path"/10.15/kextcache "$installer_volume_path"/usr/sbin
		chmod +x "$installer_volume_path"/usr/sbin/kextcache
	
	echo -e $(date "+%b %d %H:%M:%S") ${move_up}${erase_line}${text_success}"+ Patched kernel cache tool."${erase_style}


	echo -e $(date "+%b %d %H:%M:%S") ${text_progress}"> Copying patcher utilities."${erase_style}

		cp "$resources_path"/patch-unus.sh "$installer_volume_path"/usr/bin/patch-unus
		chmod +x "$installer_volume_path"/usr/bin/patch-unus

	echo -e $(date "+%b %d %H:%M:%S") ${move_up}${erase_line}${text_success}"+ Copied patcher utilities."${erase_style}
}

Update_Patched_Installer()
{
	echo -e $(date "+%b %d %H:%M:%S") ${text_progress}"> Updating patcher utilities."${erase_style}

		cp "$resources_path"/patch-unus.sh "$installer_volume_path"/usr/bin/patch-unus
		chmod +x "$installer_volume_path"/usr/bin/patch-unus

	echo -e $(date "+%b %d %H:%M:%S") ${move_up}${erase_line}${text_success}"+ Updated patcher utilities."${erase_style}
}

End()
{
	echo -e $(date "+%b %d %H:%M:%S") ${text_progress}"> Removing temporary files."${erase_style}

		Output_Off rm -R "$installer_volume_path"/tmp/*

	echo -e $(date "+%b %d %H:%M:%S") ${move_up}${erase_line}${text_success}"+ Removed temporary files."${erase_style}


	echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/ Thank you for using Catalina Unus."${erase_style}
	
	Input_On
	exit
}

Input_Off
Escape_Variables
Parameter_Variables
Path_Variables
Check_Environment
Check_Root
Check_Resources
Input_Operation
End